import React, {seState, useEffect,useMemo,  useRef} from 'react';
import { Machine,interpret,assign} from 'xstate'
import {authenticationService} from "./services";
import {isEmail} from "validator";


const usernameIsEmail = (_ctx, event) => {
  const standardEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return standardEmail.test(String(event.value).toLowerCase());
};



const loginMachine = Machine({
  id: 'login',
  initial: 'idle',
  context: {
      username: '',
      password: '',
  },
  states: {
      idle: {
          on: {
              REMOVE_EMAIL_ERROR: {
                  target: 'idle.emailErr.noError'
              },
              REMOVE_PASS_ERROR: {
                  target: 'idle.passErr.noError'
              },
              ASSIGN_USERNAME_PASSWORD:{
                    actions:'assignField'
              },
              SUBMIT: [{
                      cond: 'isEmptyEmailField',
                      target: 'idle.emailErr.errors.empty'
                  },

                  {
                      cond: 'isUsernameIsEmail',
                      target: 'idle.emailErr.errors.badFormat'
                  },
                  
                  {
                      cond: 'isEmptyPasswordField',
                      target: 'idle.passErr.errors.empty'
                  },
                  {
                      cond: 'isTooShortPasswordField',
                      target: 'idle.passErr.errors.tooShort'
                  },

                  {
                      target: 'submitAwaitingResponse'
                  }

              ]
          },


          states: {
              emailErr: {

                  initial: 'noError',
                  states: {

                      noError: {},
                      errors: {
                        initial: 'empty',
                          states: {
                              empty: {},
                              badFormat: {
                                  action:''
                              },
                              noAccount: {},

                          },

                      }

                  },

              },

              passErr: {

                  initial: 'noError',
                  states: {
                      noError: {},

                      errors: {
                          initial: 'empty',
                          states: {
                              empty: {},
                              tooShort: {},
                              incorrect: {},
                          }

                      }

                  },
              },

              serviceErr: {
                  initial: 'noError',
                  states: {
                      noError: {},

                      errors: {
                          initial: 'communication',
                          states: {
                              communication: {
                                  on: {
                                      SUBMIT: "#login.submitAwaitingResponse"
                                  }
                              },
                              internal: {}
                          }
                      }

                  },
              },
          }
      },




      submitAwaitingResponse: {
          invoke: {
              src: 'requestSignIn',
              onDone: {
                  
                  actions: 'onSuccess',
                  target:'success'


              },
              onError: [

                  {
                      cond: 'isNoAccount',
                      target: 'idle.emailErr.errors.noAccount'
                  },

                  {
                      cond: 'isIncorrectPassword',
                      target: 'idle.passErr.errors.incorrect'
                  },
                  {
                      cond: 'isNoResponse',
                      target: 'idle.serviceErr.errors.communication'
                  },
                  {
                      cond: "isInternalServerErr",
                      target: "idle.serviceErr.errors.internal"
                  }
              ]
          },
      },
      success: {
          type: 'final',
          
      },
  },
}, {
  guards: {
      isEmptyEmailField: (_, event) => event.data.email.length === 0,
      isUsernameIsEmail: (_, event) =>
          event.data.email.length > 0 && !isEmail(event.data.email),
      isEmptyPasswordField: (_, event) => event.data.password.length === 0,
      isTooShortPasswordField: (_, event) => event.data.password.length < 5,
      isNoAccount: (_, {data:{code}}) => code === 1,
      isIncorrectPassword: (_, event) => event.data.code === 2,
      isNoResponse: (_, {data:{code}}) => code === 3,
      isInternalServerErr: (_, event) => event.data.code === 4,
      



  },
  services: {
    requestSignIn: (_, event) => authenticationService(event.data.email, event.data.password)
  },
  actions: {

      onSuccess: () => {
          console.log("signed in");
      },
      assignField: assign ({
        //   username: (context, event)=>{
        //       event.target.value
        //   }
      })

  },
  
});
export default loginMachine;