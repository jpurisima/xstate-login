import React,{useState, useEffect, useMemo} from 'react';
import {Machine, interpret, assign} from 'xstate'
import { useMachine } from '@xstate/react';
import loginMachine from "../Machines/config";
import initMachineOption from "../Machines/init";



const Form = () => {

    const [machine, send] = useMachine(loginMachine);

    // *Get all inputs by event
    const [signInFormInputs, updateForm]=useState({
      email:'',
      password:''
    })

    const handleChangeValue = e =>     {
        const {name, value}= e.target
        updateForm({
            ...signInFormInputs,
            // [e.target.name]: e.target.value,
            [name]:value,
        })
        
        send({
            type: 'ASSIGN_USERNAME_PASSWORD',
            payload:{
                username:'asd',
                password :'asd'
            }
          
        });
    }


    const handleSubmitForm = e => {
        e.preventDefault();
        send({
            type: 'SUBMIT',
            // send to machine actions get by input data by events
            data: {
                ...signInFormInputs
            }
        });

        
      };



    const focusRemoveEmailError = e => {
        e.preventDefault();
        send({
            type: 'REMOVE_EMAIL_ERROR',
          
        });

        
      };


      const focusRemovePassError = e => {
        e.preventDefault();
        send({
            type: 'REMOVE_PASS_ERROR',
          
        });

        
      };

      


 

     
      
        

  
    console.log(machine.value);
    console.log(signInFormInputs);
    return (
        <div>
            <form onSubmit={handleSubmitForm} noValidate>
                
                <label htmlFor="email">Username</label>
                <input onFocus={focusRemoveEmailError} type="text" id="email" name="email" placeholder="Email" value={signInFormInputs.email} onChange={handleChangeValue}></input>
                {machine.matches("idle.emailErr.errors") ? (
                    <span className="span">
                    {machine.matches("idle.emailErr.errors.empty") &&
                        "please enter your email"}
                    {machine.matches("idle.emailErr.errors.badFormat") &&
                        "email format doesn't look right"}
                    {machine.matches("idle.emailErr.errors.noAccount") &&
                        "no account linked with this email"}
                    {machine.matches("idle.emailErr.noError") &&
                        " "}
                    
                    </span>
                ) : (
                    " "
                )}

               

                
               
                <p></p>
                <label htmlFor="password">Password</label>
                <input onFocus={focusRemovePassError} type="password" id="password" name="password" placeholder="Password" value={signInFormInputs.password} onChange={handleChangeValue}></input>
                
                {machine.matches("idle.passErr.errors") ? (
                    <span className="span"> 
                    {machine.matches("idle.passErr.errors.empty") &&
                        "please enter your password"}
                    {machine.matches("idle.passErr.errors.tooShort") &&
                        "password too short"}
                    {machine.matches("idle.passErr.errors.incorrect") &&
                        "password incorrect"}
                    {machine.matches("idle.emailErr.noError") &&
                        " "}
                   
                    </span>
                ) : (
                    " "
                )}

                
                
                <input type="submit" value="Login" isLoading={machine.matches("submitAwaitingResponse")}></input>
            </form>
        </div>
    )
}

export default Form
